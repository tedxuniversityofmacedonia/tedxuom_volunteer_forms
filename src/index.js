import React from "react";
import ReactDOM from "react-dom";
import ScrollToTop from "./components/ScrollToTop";
import "./index.css";
import App from "./App";
import "normalize.css/normalize.css";

import { BrowserRouter } from "react-router-dom";

import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(
  <BrowserRouter>
    <ScrollToTop>
      <App />
    </ScrollToTop>
  </BrowserRouter>,
  document.getElementById("root")
);
registerServiceWorker();
