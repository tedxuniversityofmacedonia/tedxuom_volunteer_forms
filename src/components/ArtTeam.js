import React from "react";
import PropTypes from "prop-types";
import Form from "./Form";
import "./TranslationTeam.css";

const ArtTeam = ({ handleDataUpdate, handleSubmit, answers, history }) => {
  return (
    <div className="translation-team">
      <div className="translation-header">
        <h2>Art Team</h2>
        <p>
          Εάν είσαι δημιουργική ψυχή και παράλληλα το άτομο που κάθε βδομάδα
          αλλάζει θέση τα έπιπλα στο σπίτι, τότε ανήκεις σε αυτήν την ομάδα! Η
          Art team, δημιουργήθηκε για την χωροθετική -καλλιτεχνική οργάνωση των
          pre-events, και φυσικά την επιμέλεια και τον σχεδιασμό των σκηνικών
          και του χώρου του μεγάλου μας event. Σε αυτή την ομάδα, θα έχεις τη
          δυνατότητα να συμβάλλεις στη δημιουργική διαδικασία της οργάνωσης του
          TEDxUniversityofMacedonia, επιστρατεύοντας τις γνώσεις, την
          ευρηματικότητα και το ταλέντο σου!
        </p>
      </div>
      <Form submit={() => handleSubmit(history)}>
        <label>
          1.Τι σε έκανε να επιλέξεις αυτή τη θέση;
          <span className="textarea-margin" />
          <textarea
            name="why_choose_position"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label>
          2.Έχεις κάποια καλλιτεχνικά ενδιαφέροντα ή εμπειρίες σε θέματα που
          αφορούν τον σχεδιασμό και τις κατασκευές;
          <span className="textarea-margin" />
          <textarea
            name="artistic_interests"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label className="choice">
          3. Γνωρίζεις κάποιο πρόγραμμα σχεδιασμού σε Η/Υ;
          <div>
            <input
              type="checkbox"
              id="programChoice1"
              name="application_experienced"
              value="Sketch up"
              onChange={handleDataUpdate}
            />
            <label htmlFor="programChoice1">Sketch up</label>
          </div>
          <div>
            <input
              type="checkbox"
              id="programChoice2"
              name="application_experienced"
              value="Rhino"
              onChange={handleDataUpdate}
            />
            <label htmlFor="programChoice2">Rhino</label>
          </div>
          <div>
            <input
              type="checkbox"
              id="programChoice3"
              name="application_experienced"
              value="Photoshop"
              onChange={handleDataUpdate}
            />
            <label htmlFor="programChoice3">Photoshop</label>
          </div>
          <div>
            <input
              type="checkbox"
              id="programChoice4"
              name="application_experienced"
              value="Autocad"
              onChange={handleDataUpdate}
            />
            <label htmlFor="programChoice4">Autocad</label>
          </div>
          <div>
            <input
              type="checkbox"
              id="programChoice5"
              name="application_experienced"
              value="Άλλο"
              onChange={handleDataUpdate}
            />
            <label htmlFor="programChoice5" id="radio_with_text">
              Άλλο
              <input
                type="text"
                name="application_experienced_other"
                onKeyUp={handleDataUpdate}
              />
            </label>
          </div>
        </label>
        <label>
          4. Παρέθεσε link με εικόνες από TEDx σκηνές που σου άρεσαν και νομίζεις ότι ταιριάζουν με την αισθητική σου.
          <span className="textarea-margin" />
          <textarea
            name="tedx_art_stages"
            onKeyUp={handleDataUpdate}
            required
          />
        </label>
        <label className="choice">
          5. Θα σε ενδιέφερε κάποια άλλη θέση στην ομάδα εθελοντών του
          TEDxUniversityofMacedonia;
          <div>
            <input
              type="radio"
              id="teamChoice1"
              name="secondary_team_choice"
              value="Blog Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice1">Blog Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice2"
              name="secondary_team_choice"
              value="Translation Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice2">Translation Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice3"
              name="secondary_team_choice"
              value="Photo Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice3">Photo Team</label>
          </div>
          <div>
            <input
              type="radio"
              id="teamChoice4"
              name="secondary_team_choice"
              value="Runners Team"
              onChange={handleDataUpdate}
              required
            />
            <label htmlFor="teamChoice4">Runners Team</label>
          </div>
        </label>
        <button type="submit">Υποβολή</button>
      </Form>
    </div>
  );
};

ArtTeam.propTypes = {
  handleDataUpdate: PropTypes.func.isRequired,
  handleSubmit: PropTypes.func.isRequired,
  answers: PropTypes.object.isRequired
};

export default ArtTeam;
