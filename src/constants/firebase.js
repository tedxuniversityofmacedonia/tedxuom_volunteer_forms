import firebase from 'firebase/app';
import 'firebase/database';

const config = {
  apiKey: "AIzaSyBXVgJ65UhSvwVcN0GTpDJI-tRsTXMWiPk",
  authDomain: "tedxuom-volunteer-fom.firebaseapp.com",
  databaseURL: "https://tedxuom-volunteer-fom.firebaseio.com",
  projectId: "tedxuom-volunteer-fom",
  storageBucket: "tedxuom-volunteer-fom.appspot.com",
  messagingSenderId: "638314354004",
};

export default firebase.initializeApp(config);